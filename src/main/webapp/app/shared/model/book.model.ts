export interface IBook {
  id?: number;
  title?: string;
  author?: string;
}

export const defaultValue: Readonly<IBook> = {};
