/**
 * View Models used by Spring MVC REST controllers.
 */
package com.seng.webapp.web.rest.vm;
